//Función pura
function add(num1, num2) {
  return num1 + num2;
}

//Siempre produce el mismo resultado para las mismas entradas.
console.log(add(1, 3)); //4
console.log(add(15, 20)); //35

//Función inpura
function addRandom(num1) {
  return num1 + Math.random();
}
//No se puede predecir el resultado de salida para una entrada dada.
console.log(addRandom(6));

let previousResult = 0;

//Side-Effects -> cambia algo fuera de la función ejem. solicitud HTTP.
function addMoreNumbers(num1, num2) {
  const sum = num1 + num2;
  previousResult = sum;
  return sum;
}
console.log(addMoreNumbers(1, 5));

const hobbies = ["Sport", "Cooking"];

function printHobbies(h) {
  h.push("NEW HOBBY");
  console.log(h);
}
printHobbies(hobbies);

//FUNCIONES DE FABRICA -> Es una función que produce otra función.
function createTaxCalculator(tax) {
  function calculateTax(amount) {
    return amount * tax;
  }
  return calculateTax;
}
//Se ejecuta por separado dos veces con diferente agurmento
const calculateVatAmount = createTaxCalculator(0.19);
const calculateIncomeTaxAmount = createTaxCalculator(0.25);

console.log(calculateVatAmount(100));
console.log(calculateIncomeTaxAmount(100));

//Cada función de javascript es Closures.
/* -> Porque cierra las variables definidas en su entorno y las memoriza
para que no se desechen cuando ya no las necesita */

let userName = "Adri";

function greetUser() {
  let name = "Adriana";
  console.log("Hi " + name);
}
let name = "Veloz";
userName = "Carmen";

greetUser();

// function powerOf(x, n) {
//     let result = 1;

//     for(let i = 0; i< n; i++) {
//         result*= x;
//     }
//     return result;
// }

//Recursividad
function powerOf(x, n) {
  // if (n === 1){
  //     return x;
  // }
  // return x * powerOf(x, n -1)
  return n === 1 ? x : x * powerOf(x, n - 1);
}

console.log(powerOf(2, 3)); //2*2*2

const myself = {
  name: "Adri",
  friends: [
    {
      name: "Veloz",
      friends: [
        {
          name: "Santos",
        },
      ],
    },
    {
        name: 'Carmen'
    }
  ],
};

function getFriendName(person) {
    const collectedNames = []

    if(!person.friends){
        return [];
    }
    for(const friend of person.friends) {
        collectedNames.push(friend.name);
        collectedNames.push(...getFriendName(friend));
    }
    return collectedNames;
}

console.log(getFriendName(myself()));
